/*
 * Speed control para el receptor Blitzrc Reciever
 *
 * Se utilizan los siguienes canales:
 *
 * AILE derecha-izierda
 * THRO adelante-atrás
 *
 * Author: Pavel E. Vázquez Martínez pavel_e@riseup.net
 *
 * Date: 05/09/2020
 *
 * Coneiones
 * ---------
 * AILE -> A5
 * THRO -> A3
 *
 * D5 -> Señal
 *
 * Note:
 * On Arduino Uno, the PWM pins are
 * 3, 5, 6, 9, 10 and 11.
 * The frequency of PWM signal on pins
 * 5 and 6 will be about 980Hz and
 * on other pins will be 490Hz
 *
 * https://gitlab.com/dpw19/speedcontrol
 *
 */

int AILE_PIN = A5;
int AILE_VALUE = 0;
int MAX_AILE_VALUE = 0;
int MIN_AILE_VALUE = 0;
int MEAN_AILE_VALUE;

int THRO_PIN = A3;
int THRO_VALUE = 0;
int MAX_THRO_VALUE = 0;
int MIN_THRO_VALUE = 0;
int MEAN_THRO_VALUE;

int DELTA = 20;

int ADELANTE = 0;
int ATRAS = 0;
int DERECHA = 0;
int IZQUIERDA = 0;

bool COMUNICATION_FLAG = false;

int S1AV = 5;
int S2AV = 6;

int S1GI = 9;
int S2GI = 10;

void setup() {
  Serial.begin(19200);
// Pines del receptor como entrada
  pinMode(AILE_PIN, INPUT);
  pinMode(THRO_PIN, INPUT);

// Pines para el driver de motores como salida
  pinMode(S1AV, OUTPUT);
  pinMode(S2AV, OUTPUT);
  pinMode(S1GI, OUTPUT);
  pinMode(S2GI, OUTPUT);

//  Lectura inicial
  THRO_VALUE = pulseIn(THRO_PIN, HIGH);
  MAX_THRO_VALUE = THRO_VALUE;
  MIN_THRO_VALUE = THRO_VALUE;

  AILE_VALUE = pulseIn(AILE_PIN, HIGH);
  MAX_AILE_VALUE = AILE_VALUE;
  MIN_AILE_VALUE = AILE_VALUE;
}

void loop() {
  lectura();
  max_min_detect();
  while(!sincronizacion());
  mapeo();
  action();

  delay(50);
}

void lectura(){
  AILE_VALUE = pulseIn(AILE_PIN, HIGH);
  THRO_VALUE = pulseIn(THRO_PIN, HIGH);
}

void max_min_detect(){
  if (AILE_VALUE > MAX_AILE_VALUE)
    MAX_AILE_VALUE = AILE_VALUE;
  if (AILE_VALUE < MIN_AILE_VALUE)
    MIN_AILE_VALUE = AILE_VALUE;

  if (THRO_VALUE > MAX_THRO_VALUE)
    MAX_THRO_VALUE = THRO_VALUE;
  if (THRO_VALUE < MIN_THRO_VALUE)
    MIN_THRO_VALUE = THRO_VALUE;
}

int sincronizacion(){
  int count = 24;
  //Espera sincronizacion
  if(pulseIn(THRO_PIN, HIGH) < 500){
    Serial.print("Esperando sincronización\n");
    while(pulseIn(THRO_PIN, HIGH) < 500){
      Serial.print(".");
      count--;
      if(count == 0){
        count = 24;
        Serial.println();
      }
    }
    COMUNICATION_FLAG = false;
    return 0;
  }
  else{
//  Renicia valores mínimos y máximos
    if(!COMUNICATION_FLAG){
      lectura();
      MIN_AILE_VALUE = AILE_VALUE;
      MAX_AILE_VALUE = AILE_VALUE;
      MAX_THRO_VALUE = THRO_VALUE;
      MIN_THRO_VALUE = THRO_VALUE;
      COMUNICATION_FLAG = true;
    }
    return 1;
  }
}

void mapeo(){
  // Valor medio
  MEAN_THRO_VALUE = (MAX_THRO_VALUE + MIN_THRO_VALUE) / 2;
  MEAN_AILE_VALUE = (MAX_AILE_VALUE + MIN_AILE_VALUE) / 2;

  //Atras
  if(THRO_VALUE > MEAN_THRO_VALUE + DELTA){
    ATRAS = map(THRO_VALUE, MEAN_THRO_VALUE, MAX_THRO_VALUE, 0, 255);
    ADELANTE = 0;
//    Serial.print("Atrás: ");
//    Serial.println(ATRAS);
  }
  //Adelante
  else if(THRO_VALUE < MEAN_THRO_VALUE - DELTA){
    ATRAS = 0;
    ADELANTE = map(THRO_VALUE, MIN_THRO_VALUE, MEAN_THRO_VALUE, 255, 0);
//    Serial.print("Adelante: ");
//    Serial.println(ADELANTE);
  }
  //Detenido
  else{
    ADELANTE = 0;
    ATRAS = 0;
//    Serial.println("DETENIDO");
  }

  // Derecha
  if(AILE_VALUE > MEAN_AILE_VALUE + DELTA){
    DERECHA = map(AILE_VALUE, MEAN_AILE_VALUE, MAX_AILE_VALUE, 0, 255);
    IZQUIERDA = 0;
//    Serial.print("DERECHA: ");
//    Serial.println(DERECHA);
  }
  // IZQUIERDA
  else if(AILE_VALUE < MEAN_AILE_VALUE - DELTA){
    DERECHA = 0;
    IZQUIERDA = map(AILE_VALUE, MIN_AILE_VALUE, MEAN_AILE_VALUE, 255, 0);
//    Serial.print("IZQUIERDA: ");
//    Serial.println(IZQUIERDA);
  }
  // Detenido
  else{
    DERECHA = 0;
    IZQUIERDA = 0;
//    Serial.println("DETENIDO");
  }
}

void action(){
  if (ADELANTE && !ATRAS){
    Serial.print("AVANZAR\n");
    analogWrite(S1AV, ADELANTE);
    digitalWrite(S2AV, false);
  }
  if (!ADELANTE && ATRAS){
    Serial.print("RETROCEDER\n");
    analogWrite(S2AV, ATRAS);
    digitalWrite(S1AV, false);
  }
  if (!ADELANTE && !ATRAS){
    Serial.print("STOP\n");
    digitalWrite(S1AV, false);
    digitalWrite(S2AV, false);
  }

  if(DERECHA && !IZQUIERDA){
    Serial.print("DERECHA\n");
    analogWrite(S1GI, DERECHA);
    digitalWrite(S2GI, false);
  }
  if(!DERECHA && IZQUIERDA){
    Serial.print("IZQUIERDA\n");
    analogWrite(S2GI, IZQUIERDA);
    digitalWrite(S1GI, false);
  }
  if(!DERECHA && !IZQUIERDA){
    Serial.print("CENTRO\n");
    digitalWrite(S1GI, false);
    digitalWrite(S2GI, false);
  }
}
